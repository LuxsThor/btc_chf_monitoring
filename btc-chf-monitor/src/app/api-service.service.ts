import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {

  chfPrice: number;

  constructor(private http: HttpClient) { }

  // Http Headers
  httpOptions = {
    headers: new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json; charset=utf-8'
    })
  }

  getPrice(): Promise<any> {
    let url = "https://blockchain.info/tobtc?currency=CHF&value=1";
    console.log("getting price at: ", url);
    return this.http.get(url)
    .toPromise()
      .then(
        //d res => console.log(res.response),
        res => res as number,
      );
  }

  getSpecificQuote(francs: number): Promise<any> {
    let url = "https://blockchain.info/tobtc?currency=CHF&value="+francs;
    console.log("getting price at: ", url);
    return this.http.get(url)
    .toPromise()
      .then(
        //d res => console.log(res.response),
        res => res as number,
      );
  }


}
