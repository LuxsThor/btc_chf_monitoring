import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiServiceService } from './../api-service.service';


@Component({
  selector: 'app-price-dashboard',
  templateUrl: './price-dashboard.component.html',
  styleUrls: ['./price-dashboard.component.css']
})
export class PriceDashboardComponent implements OnInit {

  apiEndpoint: string = "https://blockchain.info/tobtc?currency=USD&value=500";
  chfBTC: number;
  btcCHF: number

  constructor(private router: Router, private apiService: ApiServiceService,) { }

  ngOnInit() {
    this.update();
  }

  update(){
    this.apiService.getPrice().then(
      res => {
        this.chfBTC = res;
        this.btcCHF = 1*(1/res);
      }
    );
  }

}
