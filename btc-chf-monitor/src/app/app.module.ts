import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ApiServiceService } from './api-service.service';
import { AppComponent } from './app.component';

import { PriceDashboardComponent } from './price-dashboard/price-dashboard.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { RequestQuoteComponent } from './request-quote/request-quote.component';


const routes: Routes = [
  { path: 'home', component: PriceDashboardComponent},
  { path:  "", pathMatch:  "full", redirectTo:  "home"},
];


@NgModule({
  declarations: [
    AppComponent,
    PriceDashboardComponent,
    TopBarComponent,
    RequestQuoteComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
  ],
  providers: [ApiServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
